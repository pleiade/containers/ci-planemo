# Container image `ci-planemo`

Container image for continuous integration of [Common Workflow Language](https://commonwl.org) tools and workflows.
Includes a [Condqa](https://conda.io) installation in `/opt/conda`.

Linting:

```
planemo lint *.cwl
```

Testing without containers or Galaxy:

```
planemo test --no-container --engine cwltool my_workflow.cwl
```
