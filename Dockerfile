FROM ubuntu:20.04

ARG PLANEMO_CONDA_PREFIX=/opt/conda
ENV PLANEMO_CONDA_PREFIX=${PLANEMO_CONDA_PREFIX}

RUN apt-get update &&\
    apt-get install -y curl python3-pip &&\
    apt-get clean --yes

RUN pip3 install planemo

RUN planemo conda_init &&\
    ${PLANEMO_CONDA_PREFIX}/bin/conda init &&\
    . $HOME/.bashrc &&\
    conda install nodejs -y

ARG USER_ID=1000
ARG USER_NAME=planemo
ARG USER_HOME=/home/$USER_NAME

RUN useradd --uid ${USER_ID} --groups 0 --create-home ${USER_NAME} --home-dir ${USER_HOME} &&\
    chown -R ${USER_ID}:0 ${PLANEMO_CONDA_PREFIX} ${USER_HOME} &&\
    chmod -R g=u ${PLANEMO_CONDA_PREFIX} ${USER_HOME} &&\
    find ${PLANEMO_CONDA_PREFIX} ${USER_HOME} -type d -print0 | xargs -0 chmod g+ws

ENV HOME=${USER_HOME}

USER ${USER_ID}
